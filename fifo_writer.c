#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#define TMPFILE "/tmp/osfifo"

struct sigaction old_action;
int npf_fd, bytes_written;
struct timeval start, end;

void print_error(const char *msg)
{
	printf("[ERROR] %s: %s.\n", msg, strerror(errno));
}

void sigpipe_handler(int signum)
{
	// Stop time measurement and print results	
	if(gettimeofday(&end, NULL))
	{
		print_error("Could not get time");
		close(npf_fd);
		exit(errno);
	}
	double elapsed_millisec = (end.tv_sec - start.tv_sec) * 1000.0;
	elapsed_millisec += (end.tv_usec - start.tv_usec) / 1000.0;

	// Print relevant info
	printf("Duration: %f milliseconds\n", elapsed_millisec);
	printf("Total bytes written: %d\n", bytes_written);

	// Unlink
	if(unlink(TMPFILE) < 0)
	{
		print_error("Error unlinking named pipe file");
		close(npf_fd);
		exit(errno);
	}

	// Close resources
	close(npf_fd);
	if(sigaction(SIGINT, &old_action, NULL) != 0)
	{
		print_error("Signal handle registration failed");
		exit(errno);
	}	

	exit(-1);
}

int main(int argc, char **argv)
{
	// Handle SIGPIPE
	struct sigaction pipe_action;
	pipe_action.sa_handler = sigpipe_handler;
	pipe_action.sa_flags = 0;
	if(sigaction(SIGPIPE, &pipe_action, NULL) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

	// Un-handle SIGINT
	struct sigaction new_action;
	new_action.sa_handler = SIG_IGN;
	if(sigaction(SIGINT, &new_action, &old_action) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

	// Check for valid command-line arguments, and assign into variables
	if(argc != 2)
	{
		printf("Invalid command-line arguments!\n");
		return -1;
	}
	int num = strtol(argv[1], NULL, 10);

	// Check if file exists
	struct stat64 npf_st;
	if(stat64(TMPFILE, &npf_st) == 0)
	{
		// Assume fifo file exists, unlink it
		if(unlink(TMPFILE) < 0)
		{
			print_error("Failed to unlink named pipe file");
			return errno;
		}
	}

	// Create named pipe file and open it
	if(mkfifo(TMPFILE, 0600) < 0)
	{
		print_error("mkfifo failed");
		return errno;
	}
	npf_fd = open(TMPFILE, O_WRONLY);
	if(npf_fd < 0)
	{
		print_error("Failed to open named pipe file");
		return errno;
	}

	// Start time measurement
	if(gettimeofday(&start, NULL))
	{
		print_error("Could not get time");
		close(npf_fd);
		return errno;
	}

	// Write to file
	int i, tmp;
	bytes_written = 0;
	char a_buffer[4096];
	for(i = 0; i < 4096; i++)
		a_buffer[i] = 'a';

	while(bytes_written < num)
	{
		tmp = write(npf_fd, (const void*)a_buffer, 4096);
		if (tmp < 0)
		{
			print_error("write failed");
			close(npf_fd);
			return errno;
		}
		bytes_written += tmp;
	}

	// End time measurement and calculate duration
	if(gettimeofday(&end, NULL))
	{
		print_error("Could not get time");
		close(npf_fd);
		return errno;
	}

	double elapsed_millisec = (end.tv_sec - start.tv_sec) * 1000.0;
	elapsed_millisec += (end.tv_usec - start.tv_usec) / 1000.0;

	// Print relevant info
	printf("Duration: %f milliseconds\n", elapsed_millisec);
	printf("Total bytes written: %d\n", bytes_written);

	// Unlink
	if(unlink(TMPFILE) < 0)
	{
		print_error("Error unlinking named pipe file");
		close(npf_fd);
		return errno;
	}

	// Close resources
	close(npf_fd);
	if(sigaction(SIGINT, &old_action, NULL) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}	

	return 0;
}