#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <errno.h>

#define TMPFILE "/tmp/mmapped.bin"

void print_error(const char *msg)
{
	printf("[ERROR] %s: %s.\n", msg, strerror(errno));
}

int main(int argc, char **argv)
{
	// Un-handle SIGTERM
	struct sigaction new_action, old_action;
	new_action.sa_handler = SIG_IGN;
	if(sigaction(SIGTERM, &new_action, &old_action) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

	// Check for valid command-line arguments, and assign them into variables
	if(argc != 3)
	{
		printf("Invalid command-line arguments!\n");
		return -1;
	}
	int num = strtol(argv[1], NULL, 10);
	pid_t rpid = strtol(argv[2], NULL, 10);

	// Create tmp file
	int tmp_fd = open(TMPFILE, O_RDWR | O_CREAT | O_TRUNC, 0600);
	if(tmp_fd < 0)
	{
		print_error("Could not open tmp file");
		return errno;
	}

	// Force the file to be of the same size as the (mmapped) array
	int result = lseek(tmp_fd, num - 1, SEEK_SET);
	if (-1 == result)
	{
		print_error("Error calling lseek() to 'stretch' the file");
		close(tmp_fd);
		return errno;
	}

	// Something has to be written at the end of the file,
	// so the file actually has the new size. 
	result = write(tmp_fd, "", 1);
	if (result != 1)
	{
		print_error("Error writing last byte of the file");
		close(tmp_fd);
		return errno;
	}

	// Create memory map
	char *array = (char *)mmap(NULL, num, PROT_READ | PROT_WRITE, MAP_SHARED, tmp_fd, 0);
	if(array == MAP_FAILED)
	{
		print_error("Error mmapping the file");
		close(tmp_fd);
		return errno;
	}

	// Start time measurement
	struct timeval start;
	if(gettimeofday(&start, NULL))
	{
		print_error("Could not get time");
		munmap(array, num);
		close(tmp_fd);
		return errno;
	}

	// Fill array
	int bytes_written = 0, i;
	for(i = 0; i < num - 1; i++)
	{
		array[i] = 'a';
		bytes_written++;
	}
	array[num - 1] = 0;

	// Kill RPID
	int kres = kill(rpid, SIGUSR1);
	if(kres < 0)
	{
		print_error("Error killing RPID");
		munmap(array, num);
		close(tmp_fd);
		return errno;
	}

	// End time measurement and calculate duration
	struct timeval end;
	if(gettimeofday(&end, NULL))
	{
		print_error("Could not get time");
		munmap(array, num);
		close(tmp_fd);
		return errno;
	}
	double elapsed_millisec = (end.tv_sec - start.tv_sec) * 1000.0;
	elapsed_millisec += (end.tv_usec - start.tv_usec) / 1000.0;

	// Print relevant info
	printf("Duration: %f milliseconds\n", elapsed_millisec);
	printf("Total bytes written: %d\n", bytes_written);

	// Close resources
	if(munmap(array, num) < 0)
	{
		print_error("Error un-mmapping the file");
		close(tmp_fd);
		return errno;
	}
	close(tmp_fd);
	if(sigaction(SIGTERM, &old_action, NULL) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}	

	return 0;
}