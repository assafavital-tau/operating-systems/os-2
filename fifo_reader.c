#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>

#define TMPFILE "/tmp/osfifo"

void print_error(const char *msg)
{
	printf("[ERROR] %s: %s.\n", msg, strerror(errno));
}

int main()
{
	// Sleep for a bit...
	sleep(1);

	// Un-handle SIGINT
	struct sigaction new_action, old_action;
	new_action.sa_handler = SIG_IGN;
	if(sigaction(SIGINT, &new_action, &old_action) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

	// Open named pipe file and determine size
	int npf_fd = open(TMPFILE, O_RDONLY);
	if(npf_fd < 0)
	{
		print_error("Failed to open named pipe file");
		return errno;
	}

	// Start time measurement
	struct timeval start;
	if(gettimeofday(&start, NULL))
	{
		print_error("Could not get time");
		close(npf_fd);
		return errno;
	}

	// Read from file
	int i, bytes_counted = 0, tmp, j;
	char byte[1024];
	while((tmp = read(npf_fd, byte, 1024)) != 0)
	{
		for(j = 0; j < tmp; j++)
			if(byte[j] == 'a') bytes_counted++;
	}

	// End time measurement and calculate duration
	struct timeval end;
	if(gettimeofday(&end, NULL))
	{
		print_error("Could not get time");
		close(npf_fd);
		return errno;
	}
	double elapsed_millisec = (end.tv_sec - start.tv_sec) * 1000.0;
	elapsed_millisec += (end.tv_usec - start.tv_usec) / 1000.0;

	// Print relevant info
	printf("Duration: %f milliseconds\n", elapsed_millisec);
	printf("Total 'a' bytes counted: %d\n", bytes_counted);

	// Close resources
	close(npf_fd);
	if(sigaction(SIGINT, &old_action, NULL) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

	return 0;
}