#define __USE_LARGEFILE64
#define _LARGEFILE_SOURCE
#define _LARGEFILE64_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <string.h>

#define TMPFILE "/tmp/mmapped.bin"

struct sigaction usr_old_action, term_old_action;

void print_error(const char *msg)
{
	printf("[ERROR] %s: %s.\n", msg, strerror(errno));
}

// SIGUSR1 signal handler
void my_signal_handler(int signum)
{
	// Open tmp file
	int tmp_fd = open(TMPFILE, O_RDWR , 0600);
	if(tmp_fd < 0)
	{
		print_error("Could not open tmp file");
		exit(errno);
	}

	// Get file size
	struct stat64 tmp_st;
	if(stat64(TMPFILE, &tmp_st) < 0)
	{
		print_error("Could not determine file size");
		exit(errno);
	}
	size_t tmp_size = tmp_st.st_size;

	// Start time measurement
	struct timeval start;
	if(gettimeofday(&start, NULL))
	{
		print_error("Could not get time");
		exit(errno);
	}

	// Create memory map
	char *array = (char *)mmap(NULL, tmp_size, PROT_READ | PROT_WRITE, MAP_SHARED, tmp_fd, 0);
	if(array == MAP_FAILED)
	{
		print_error("Error mmapping the file");
		close(tmp_fd);
		exit(errno);
	}

	// Counting bytes
	int bytes_counted = 0, i = 0;
	while(array[i] != 0)
	{
		if(array[i] == 'a') bytes_counted++;
		i++;
	}

	// End time measurement and calculate duration
	struct timeval end;
	if(gettimeofday(&end, NULL))
	{
		print_error("Could not get time");
		exit(errno);
	}
	double elapsed_millisec = (end.tv_sec - start.tv_sec) * 1000.0;
	elapsed_millisec += (end.tv_usec - start.tv_usec) / 1000.0;

	// Print relevant info
	printf("Duration: %f milliseconds\n", elapsed_millisec);
	printf("Total 'a' bytes counted: %d\n", bytes_counted);

	// Unlink
	if(unlink(TMPFILE) < 0)
	{
		print_error("Error unlinking tmp file");
		close(tmp_fd);
		exit(errno);
	}

	// Close resources
	if(munmap(array, tmp_size) < 0)
	{
		print_error("Error un-mmapping the file");
		close(tmp_fd);
		exit(errno);
	}
	close(tmp_fd);

	// Restore signal handler
  	if(sigaction(SIGUSR1, &usr_old_action, NULL) != 0)
  	{
 	   print_error("Signal handle registration failed");
 	   exit(errno);
 	}
   	if(sigaction(SIGTERM, &term_old_action, NULL) != 0)
  	{
 	   print_error("Signal handle registration failed");
 	   exit(errno);
 	}

	exit(0);
}

int main()
{
	// Un-handle SIGTERM
	struct sigaction term_new_action;
	term_new_action.sa_handler = SIG_IGN;
	if(sigaction(SIGTERM, &term_new_action, &term_old_action) != 0)
	{
		print_error("Signal handle registration failed");
		return errno;
	}

  	// Structure to pass to the registration syscall
  	struct sigaction usr_new_action;

  	// Assign pointer to our handler function
  	usr_new_action.sa_handler = my_signal_handler;

  	// Remove any special flag
  	usr_new_action.sa_flags = 0;

  	// Register the handler
  	if (sigaction(SIGUSR1, &usr_new_action, &usr_old_action) != 0)
  	{
	    print_error("Signal handle registration failed");
	    return errno;
  	}

  	// Meditate untill killed
  	while(1)
  	{
	    sleep(2);
	    // printf("Process runs.\n");
  	}
  	return 0;
}